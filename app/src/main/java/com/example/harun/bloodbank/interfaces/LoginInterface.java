package com.example.harun.bloodbank.interfaces;

public interface LoginInterface {

    void goRegister();
    void goRememberPassword();
    void goMainPage();
    void goLogin();


}
