package com.example.harun.bloodbank;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.database.Database;
import com.example.harun.bloodbank.fragments.FragmentMainPage;
import com.example.harun.bloodbank.fragments.FragmentMessages;
import com.example.harun.bloodbank.interfaces.ProfileInterface;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {
    Dialog myDialog;



    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();

    FragmentManager manager;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        loadData();

        FragmentMainPage fmp = new FragmentMainPage();
        fmp.sendData(wantedBloodList);
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.mp,fmp);
        transaction.addToBackStack("fragMP");
        transaction.commit();
        final BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

                Intent url;
                if(menuItem.getItemId() == R.id.profile) {
                    url = new Intent(MainPage.this,Profile.class);
                    startActivity(url);
                    return;
                } else if(menuItem.getItemId() == R.id.messages){
                    Toast.makeText(MainPage.this,"Bu sayfada yakınınızda acile kana ihtiyacı olanları görebilirsiniz.",Toast.LENGTH_SHORT).show();
                    FragmentMessages fm = new FragmentMessages();
                    Log.d("List",wantedBloodList.toString());
                    fm.sendData(wantedBloodList);
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.mp,fm);
                    transaction.addToBackStack("fragMessages");
                    transaction.commit();
                    return;
                }else if(menuItem.getItemId() == R.id.home){
                    FragmentMainPage fmp = new FragmentMainPage();
                    fmp.sendData(wantedBloodList);
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.mp,fmp);
                    transaction.commit();
                    return;

                }else  {
                    myDialog = new Dialog(MainPage.this);
                    ShowPopup();
                    return;
                }

            }
        });


    }

    public void ShowPopup(){
        myDialog.setContentView(R.layout.custompopup);
        TextView tv = (TextView)myDialog.findViewById(R.id.close);
        final TextView fullName = (TextView)myDialog.findViewById(R.id.fullName);
        final TextView phoneNumber = (TextView)myDialog.findViewById(R.id.phoneNumber);
        final Spinner city = (Spinner)myDialog.findViewById(R.id.city);
        final Spinner town = (Spinner)myDialog.findViewById(R.id.town);
        final Spinner blood = (Spinner)myDialog.findViewById(R.id.bloodType);
        Button btn = (Button)myDialog.findViewById(R.id.startFinding);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database db = new Database(MainPage.this);
                db.insertData(new WantedBlood(fullName.getText().toString(),
                        phoneNumber.getText().toString(),
                        city.getSelectedItem().toString(),
                        town.getSelectedItem().toString(),
                        blood.getSelectedItem().toString()
                ));
                Toast.makeText(MainPage.this,"Kan aranmaya başlandı. Aradığınız acil kanları profil sayfanızda görebilirsiniz.",Toast.LENGTH_LONG).show();
                myDialog.dismiss();
            }
        });

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    public void loadData() {
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Kaan Burkay","0535 021 40 33","Bursa","İnegöl","AB RH+"));
        wantedBloodList.add(new WantedBlood("Muammer Akal","0531 432 26 19","Malatya","Kayısısı","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
    }

    public void onBackPressed(){
        int elemanSayisi = manager.getBackStackEntryCount();
        if(elemanSayisi == 1)
            moveTaskToBack(true);
        else
            manager.popBackStack();
    }

    @Override
    public void onBackStackChanged() {

    }
}
