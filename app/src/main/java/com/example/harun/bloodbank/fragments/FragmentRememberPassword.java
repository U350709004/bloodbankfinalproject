package com.example.harun.bloodbank.fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.R;

import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRememberPassword extends Fragment implements View.OnClickListener {

    TextView tvEmailAdress,tvPhoneNumber;
    TextInputLayout tiEmail,thiPhoneNumber;
    View v;
    public FragmentRememberPassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         v = inflater.inflate(R.layout.fragment_fragment_remember_password, container, false);
         loadData();
         ((Button)v.findViewById(R.id.sendPW)).setOnClickListener(this);
         return v;
    }

    private void loadData() {
        tvEmailAdress = (TextView)v.findViewById(R.id.tvEmailAdress);
        tvPhoneNumber = (TextView)v.findViewById(R.id.tvPhoneNumber);

        tiEmail = (TextInputLayout)v.findViewById(R.id.inputLayoutEmailAdress);
        thiPhoneNumber = (TextInputLayout)v.findViewById(R.id.inputLayoutPhoneNumber);
    }

    private boolean EmailControl() {
        if(tvEmailAdress.getText().toString().trim().isEmpty()){
            tiEmail.setError("E mail alanı boş bırakılamaz");
            return false;
        }else if(!isValidEmailId(tvEmailAdress.getText().toString().trim())){
            tiEmail.setError("Geçerli bir email adresi giriniz");
            return false;
        }else {
            tiEmail.setErrorEnabled(false);
            return true;
        }
    }

    private boolean PhoneControl() {
        if(tvPhoneNumber.getText().toString().isEmpty()){
            thiPhoneNumber.setError("Telefon alanı boş bırakılamaz");
            return false;
        }else {
            thiPhoneNumber.setErrorEnabled(false);
            return true;
        }
    }

    private boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @Override
    public void onClick(View v) {
        if(EmailControl() && PhoneControl()){
            Toast.makeText(getActivity(),"Geçici Şifreniz E mailinize gönderildi",Toast.LENGTH_SHORT).show();
        }
    }


}
